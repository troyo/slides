# slides

This is a template for projects with presentations or slides.

It holds plenty of slides giving a small glimpse of what is possible.

It uses [reveal.js](https://revealjs.com/) as foundation - if you have questions about how to do something - please see there...

The presentation skeleton is done in a way that you only have to write markdown to fill it: You have to edit [slides.md](slides.md) to do so. 
But it is of course possible to unleash the full power of
[reveal.js](https://revealjs.com/) by editing [index.html](index.html).

Whenever you commit changes to the project, it is rebuilt via a CI/CD pipeline so that the live version (uses pages functionality) is always up to date.

You will have to edit the link to the live presentation here:

[Live](https://elbosso.gitlab.io/slides/)


